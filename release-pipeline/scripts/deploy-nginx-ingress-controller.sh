#!/bin/sh
cd release-pipeline/scripts
source environment.sh

echo " \
kops export kubecfg ${CLUSTER_NAME} \
  --state=${KOPS_STATE_STORE} \
"
kops export kubecfg ${CLUSTER_NAME} \
  --state=${KOPS_STATE_STORE}

echo " \
kubectl apply -f ../ingress-nginx/deploy.yaml
"
kubectl apply -f ../ingress-nginx/deploy.yaml
