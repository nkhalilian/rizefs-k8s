#!/bin/sh

cd scripts
source environment.sh

echo " \
kops delete cluster $CLUSTER_NAME \
  --state=$KOPS_STATE_STORE \
  --yes
"

kops delete cluster ${CLUSTER_NAME} \
  --state=${KOPS_STATE_STORE} \
  --yes
cd ..
