# RizeFS-K8s
Deploy 2 microservices on a cluster of EC2 managed by Kubernetes.

This project consists of two microservices that are deployed to a cluster of EC2 compute units. Used technologies are:
- GitLab code and image repository
- Gitlab deployment pipeline
- Docker to containerize the microservices
- Kubernetes to orchestrate the containers on cluster of Amazon EC2 instances
- kOps to provision and configure the Kubernetes cluster
- nginx as load balancer

## Environment
The project structure is as follow:
- build-docker-img
<pre> Contain the code for the both microservices (sinatra and webrick), Dockerfiles, puma ruby file to run sinatra on puma web server instead of the default webrick </pre>
- release-pipeline
<pre>
    cluster -> k8s manifest for provisioning the cluster (VPC, Sunbets, master/worker K8s nodes)
    dashboard -> K8s dashboard for visually checking on the cluster (deployments, pods, services)
    deployments -> deployment manifest for the 2 docker images (sinatra and webrick) 
    docker -> docker container to run on the nodes to install all the dependencies such as Kubectl and kOps
    ingress-nginx -> nginx k8s manifest to deploy the nginx components such as the controller and load balancer. The domain certificate is obtained from Amazon and has been added to ervice.beta.kubernetes.io/aws-load-balancer-ssl-cert
    scripts -> contains the scripts that are invoked in the gitlab pipeline to build the code, push the image, provision the cluster and deploy to the cluster
    services -> contains 2 services for sinatra and webrick along with the nginx ingress service to direct the incoming trrafic to load balancer to:
        www.niloufarkhalilian.com/sinatra -> sinatra service
        www.niloufarkhalilian.com/webrick -> webrick service

</pre>
- .gitlab-ci.yml -> The gitlab pipeline yaml file to define the pipeline and deployment logic. The commit to the repository automatically triggers the deployment.



## Usage
If client requests www.niloufarkhalilian.com/sinatra in the browser, then should be directed to a page that says "Rize and shine!". If client requests www.niloufarkhalilian.com/webrick, then should be directed to a page that says "Money done right"



## TODO
Add a logic for directing the trrafic to different ports based on a defined schedule. The ports 1212 and 206 are exposed on both of the containers (sinatra and webrick), however, currently load balancer only sends the load to fixed ports sinatra:1212 and webrick:206. More reseach on nginx controller might help with implementing this logic.



