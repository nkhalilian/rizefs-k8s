#!/bin/sh
cd release-pipeline/scripts
source environment.sh

echo " \
kops export kubecfg ${CLUSTER_NAME} \
  --state=${KOPS_STATE_STORE} --admin \
"
kops export kubecfg ${CLUSTER_NAME} \
  --state=${KOPS_STATE_STORE} --admin

#echo " \
#kubectl create -f ../secrets/${CLUSTER_NAME}-secret.yaml --dry-run=true -o yaml | kubectl apply -f -
#"
#kubectl create -f ../secrets/${CLUSTER_NAME}-secret.yaml --dry-run=true -o yaml | kubectl apply -f -


# echo " \
# kubectl create -f ../config-maps/${APPLICATION_NAME}-${ENV_NAME}-config-map.yaml --dry-run=true -o yaml | kubectl apply -f -
# "
# kubectl create -f ../config-maps/${APPLICATION_NAME}-${ENV_NAME}-config-map.yaml --dry-run=true -o yaml | kubectl apply -f -



echo " \
kubectl create secret docker-registry regcred --docker-server=${CI_REGISTRY} --docker-username=${GITLAB_USER_LOGIN}  \
--docker-password=${CI_REGISTRY_ACCESS_TOKEN} --docker-email=${GITLAB_USER_EMAIL}  \
 --dry-run=client -o yaml | kubectl apply -f -
"
kubectl create secret docker-registry regcred --docker-server=${CI_REGISTRY} --docker-username=${GITLAB_USER_LOGIN}  \
--docker-password=${CI_REGISTRY_ACCESS_TOKEN} --docker-email=${GITLAB_USER_EMAIL}  \
 --dry-run=client -o yaml | kubectl apply -f -
kubectl get secret regcred --output=yaml
#--namespace=${APPLICATION_NAME}-${ENV_NAME} 
echo " \
kubectl create -f ../deployments/${APPLICATION_NAME}-${ENV_NAME}-deployment.yaml --dry-run=client -o yaml | kubectl apply -f -
"
kubectl create -f ../deployments/${APPLICATION_NAME}-${ENV_NAME}-deployment.yaml --dry-run=client -o yaml | kubectl apply -f -


echo " \
kubectl create -f ../services/${APPLICATION_NAME}-${ENV_NAME}-service.yaml --dry-run=client -o yaml | kubectl apply -f -
"
kubectl create -f ../services/${APPLICATION_NAME}-${ENV_NAME}-service.yaml --dry-run=client -o yaml | kubectl apply -f -

cd ..
