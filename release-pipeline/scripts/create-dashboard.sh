#!/bin/sh
cd release-pipeline/scripts
source environment.sh

echo " \
kops export kubecfg ${CLUSTER_NAME} \
  --state=${KOPS_STATE_STORE} \
"
kops export kubecfg ${CLUSTER_NAME} \
  --state=${KOPS_STATE_STORE}

kubectl apply -f ../dashboard/${CLUSTER_NAME}-dashboard.yaml
kops get secrets kube --type secret -oplaintext
echo "ACCESS URL ---> https://k8s.api.${CLUSTER_NAME}/api/v1/namespaces/kubernetes-dashboard/services/https:kubernetes-dashboard:/proxy/."
echo "TOKEN --->"
kubectl -n kubernetes-dashboard describe secret $(kubectl -n kubernetes-dashboard get secret | grep admin-user | awk '{print $1}')