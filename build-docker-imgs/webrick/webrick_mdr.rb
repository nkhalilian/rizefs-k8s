require 'webrick'
WEBrick::HTTPServer.new(:Port => ARGV[0]).tap {|srv|
    srv.mount_proc('/') {|request, response| response.body = "Money done right"}
}.start
