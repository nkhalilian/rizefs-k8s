#!/bin/sh
cd release-pipeline/scripts
kops version
kubectl version
if sh create-cluster.sh ; then
    echo "Success"
else
    sh update-cluster.sh
fi

source environment.sh

echo " \
kops export kubecfg --name ${CLUSTER_NAME} \
  --state ${KOPS_STATE_STORE} --admin \
"
kops export kubecfg --name ${CLUSTER_NAME} \
  --state ${KOPS_STATE_STORE} --admin

kops version
kubectl version

until kops validate cluster;
do
  timestamp=$(date +"%T")
  echo "${timestamp}: Cluster is not ready..."
  sleep 60
done
cd ..

